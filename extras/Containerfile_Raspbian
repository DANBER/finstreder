FROM docker.io/yummygooey/raspbian-buster:latest

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y --no-install-recommends nano wget curl
RUN apt-get install -y --no-install-recommends git
RUN apt-get install -y --no-install-recommends python3-pip python3-dev

# Special raspbian installations
RUN apt-get install -y --no-install-recommends libffi-dev
RUN apt-get install -y --no-install-recommends build-essential
RUN apt-get install -y --no-install-recommends libssl-dev
RUN apt-get install -y --no-install-recommends python3-wheel

# Update pip and setuptools
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir --upgrade setuptools

# Special requirement for raspbian, as it's not preinstalled
RUN apt-get install -y --no-install-recommends gcc

# Install openfst with python bindings
RUN wget http://www.openfst.org/twiki/pub/FST/FstDownload/openfst-1.8.1.tar.gz
RUN tar -xzf openfst-*.tar.gz --no-same-owner && rm openfst-*.tar.gz
RUN cd openfst-*/ && ./configure \
  --enable-python --enable-lookahead-fsts --enable-static=no --enable-far
RUN cd openfst-*/ && make LDFLAGS='-latomic' -j $(nproc); make install -j $(nproc)
# Link files that they can be globaly used
RUN ln -s /usr/local/lib/libfst* /usr/lib/
RUN ln -s /usr/local/lib/fst/* /usr/lib/
RUN ln -s /usr/local/include/fst /usr/include/
RUN ln -s /usr/local/lib/python3.7/site-packages/* /usr/local/lib/python3.7/dist-packages/

# Install opengrm
RUN wget http://www.opengrm.org/twiki/pub/GRM/NGramDownload/ngram-1.3.13.tar.gz
RUN tar -xzf ngram-*.tar.gz --no-same-owner && rm ngram-*.tar.gz
RUN cd ngram-*/ && ./configure \
  --enable-static=no
RUN cd ngram-*/ && make -j $(nproc); make install -j $(nproc)
# Link files that they can be globaly used
RUN ln -s /usr/local/lib/libngram* /usr/lib/

# Install python testing tools
RUN pip3 install --upgrade --no-cache-dir \
  pytest \
  pytest-cov
RUN pip3 freeze

# Preinstall finstreder's python dependencies
COPY ./ /finstreder/
RUN pip3 install --upgrade --no-cache-dir --user -e /finstreder/
RUN pip3 freeze

# Clear cache to save space, only has an effect if image is squashed
RUN apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /
CMD ["/bin/bash"]
