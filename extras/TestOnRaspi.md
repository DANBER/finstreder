# Testing finstreder on RaspberryPi

- Enable cross-building for other architectures: \
   (You have to rerun the first command after every restart of your computer)

  ```bash
  sudo podman run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
  podman build -f extras/Containerfile_Raspbian -t finstreder_raspbian .
  ```

- Run test on cross-build machine:

  ```bash
  podman run --network host --rm \
    --volume `pwd`/:/finstreder/ \
    -it finstreder_raspbian \
      pytest /finstreder/ --cov=finstreder
  ```

- Convert container image to single file:

```bash
   podman save -o finstrp.tar --format oci-archive "finstreder_raspbian"
   gzip -9 -f "finstrp.tar"
```

- Copy to raspi and load it there:

```bash
   gunzip "finstrp.tar.gz"

   podman load -i finstrp.tar
   nohup podman load -i finstrp.tar > nohup.out 2>&1 &
```

- Run above test again
