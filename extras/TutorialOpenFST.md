# OpenFST Tutorial Help

Follow the [QuickTour](http://www.openfst.org/twiki/bin/view/FST/FstQuickTour) and the official [Examples](http://www.openfst.org/twiki/bin/view/FST/FstExamples) readmes from OpenFST. \
This readme only collects some missing and some additional commands.

Tokenize example:

```bash
# Martian and Man FSTs
fstcompile --isymbols=ascii.syms --osymbols=wotw.syms > Martian.fst <<EOF
0 1 M Martian
1 2 a <epsilon>
2 3 r <epsilon>
3 4 t <epsilon>
4 5 i <epsilon>
5 6 a <epsilon>
6 7 n <epsilon>
7
EOF
fstcompile --isymbols=ascii.syms --osymbols=wotw.syms > man.fst <<EOF
0 1 m man
1 2 a <epsilon>
2 3 n <epsilon>
3
EOF

# Combinations and Optimizations
fstunion man.fst Mars.fst | fstunion - Martian.fst | fstclosure > lexicon.fst
fstdraw --isymbols=ascii.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait lexicon.fst | dot -Tjpg > lexicon.jpg

fstrmepsilon lexicon.fst | fstdeterminize | fstminimize > lexicon_opt1.fst
fstdraw --isymbols=ascii.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait lexicon_opt1.fst | dot -Tjpg > lexicon_opt1.jpg
fstdraw --isymbols=ascii.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait punct.fst | dot -Tjpg > punct.jpg

fstunion man.fst Mars.fst | fstunion - Martian.fst | fstconcat - punct.fst | fstclosure > lexicon2.fst
fstrmepsilon lexicon2.fst | fstdeterminize | fstminimize > lexicon_opt2.fst
fstdraw --isymbols=ascii.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait lexicon_opt2.fst | dot -Tjpg > lexicon_opt2.jpg

# Marsman acceptor (input)
fstcompile --isymbols=ascii.syms --acceptor > Marsman.fst <<EOF
0 1 M
1 2 a
2 3 r
3 4 s
4 5 <space>
5 6 m
6 7 a
7 8 n
8 9 !
9
EOF
fstdraw --isymbols=ascii.syms --osymbols=ascii.syms --width=120.0 --height=20.0 -portrait Marsman.fst | dot -Tjpg > Marsman.jpg

# Combine to tokens
fstcompose Marsman.fst lexicon_opt2.fst | fstproject --project_type="output" | fstrmepsilon > tokens.fst
fstdraw --isymbols=wotw.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait tokens.fst | dot -Tjpg > tokens.jpg
```

Exercise 1:

```bash
# Small num2word fst
fstcompile --isymbols=ascii.syms --osymbols=wotw.syms > num2word.fst <<EOF
0 2 4 four
2 3 <epsilon> thousand
3 4 2 two
4 5 <epsilon> hundred
5 6 2 twenty
6 1 5 five
0 7 2 twenty
7 1 3 three
1
EOF
fstdraw --isymbols=ascii.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait num2word.fst | dot -Tjpg > num2word.jpg

# Input fst
fstcompile --isymbols=ascii.syms --acceptor > MarsMiles.fst <<EOF
0 1 M
1 2 a
2 3 r
3 4 s
4 5 <space>
5 6 4
6 7 2
7 8 2
8 9 5
9 10 <space>
10 11 m
11 12 i
12 13 l
13 14 e
14 15 s
15
EOF
fstdraw --isymbols=ascii.syms --osymbols=ascii.syms --width=120.0 --height=20.0 -portrait MarsMiles.fst | dot -Tjpg > MarsMiles.jpg

# Other words fst, also includes transition to ignore a space
fstcompile --isymbols=ascii.syms --osymbols=wotw.syms > miles.fst <<EOF
0 1 m miles
1 2 i <epsilon>
2 3 l <epsilon>
3 4 e <epsilon>
4 5 s <epsilon>
0 1 <space> <epsilon>
5
EOF
fstunion miles.fst Mars.fst | fstunion - num2word.fst | fstclosure > lexicon3.fst
fstdraw --isymbols=ascii.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait lexicon3.fst | dot -Tjpg > lexicon3.jpg

# Compose
fstcompose MarsMiles.fst lexicon3.fst | fstprint
fstcompose MarsMiles.fst lexicon3.fst | fstproject --project_type="output" | fstrmepsilon > numwords.fst
fstdraw --isymbols=wotw.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait numwords.fst | dot -Tjpg > numwords.jpg
```

Exercise 2a:

```bash
# Capitalize fst
fstcompile --isymbols=ascii.syms --osymbols=ascii.syms > up1c.fst <<EOF
0 1 a A
0 1 b B
1 1 a a
1 1 b b
1 2 <epsilon> <epsilon>
2 0 <space> <space>
1
EOF
fstdraw --isymbols=ascii.syms --osymbols=ascii.syms --width=120.0 --height=20.0 -portrait up1c.fst | dot -Tjpg > up1c.jpg

# Input fst
fstcompile --isymbols=ascii.syms --acceptor > lowinp.fst <<EOF
0 1 a
1 2 b
2 3 b
3 4 a
4 5 <space>
5 6 b
6 7 a
7
EOF
fstdraw --isymbols=ascii.syms --osymbols=ascii.syms --width=120.0 --height=20.0 -portrait lowinp.fst | dot -Tjpg > lowinp.jpg

# Compose
fstcompose lowinp.fst up1c.fst | fstprint
fstcompose lowinp.fst up1c.fst | fstproject --project_type="output" | fstrmepsilon > capt.fst
fstdraw --isymbols=ascii.syms --osymbols=ascii.syms --width=120.0 --height=20.0 -portrait capt.fst | dot -Tjpg > capt.jpg
```

CaseRestoration example:

```bash
# Compile lm to fst
fstcompile --isymbols=wotw.syms --osymbols=wotw.syms > wotw.lm.fst < wotw.lm

# Compile downcase fst and build lower marsman example fst
fstcompile --isymbols=ascii.syms --osymbols=ascii.syms > full_downcase.fst < full_downcase.txt
fstproject Marsman.fst | fstcompose - full_downcase.fst | fstproject --project_type="output" > marsman.fst
fstdraw --isymbols=ascii.syms --osymbols=ascii.syms --width=120.0 --height=20.0 -portrait marsman.fst | dot -Tjpg > marsman.jpg

# Compile full lexicon to fst
fstcompile --isymbols=ascii.syms --osymbols=wotw.syms > lexicon_opt.fst < lexicon_opt.txt

# Build wotw.fst as explained
# Trying the first example will take a while (returned with an error after ~1.5h), so use the final one

fstcompose marsman.fst case_restore.fst | fstshortestpath | fstproject --project_type="output" | fstrmepsilon | fsttopsort > prediction.fst
fstdraw --isymbols=wotw.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait prediction.fst | dot -Tjpg > prediction.jpg
```

Exercise 5a+6a:

```bash
fstcompose marsman.fst case_restore.fst | fstshortestpath --nshortest=2 | fstproject --project_type="output" | fstrmepsilon | fsttopsort > prediction.fst
fstdraw --isymbols=wotw.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait prediction.fst | dot -Tjpg > prediction.jpg

# Marsman with spelling error
fstcompile --isymbols=ascii.syms --acceptor > maasman.fst <<EOF
0 1 m
1 2 a
2 3 a
3 4 s
4 5 <space>
5 6 m
6 7 a
7 8 n
8 9 !
9
EOF
fstdraw --isymbols=ascii.syms --osymbols=ascii.syms --width=120.0 --height=20.0 -portrait maasman.fst | dot -Tjpg > maasman.jpg

fstcompose maasman.fst case_restore.fst | fstshortestpath | fstproject --project_type="output" | fstrmepsilon | fsttopsort > prediction2.fst
fstdraw --isymbols=wotw.syms --osymbols=wotw.syms --width=120.0 --height=20.0 -portrait prediction2.fst | dot -Tjpg > prediction2.jpg
```
