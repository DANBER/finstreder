import argparse
import json
import os
import re
from typing import List

import tqdm

# ==================================================================================================

output_dirpath = "sents/"

# ==================================================================================================


def inject_alternatives(sentence: str) -> List[str]:
    """Create list of sentences with every possible alternative word combination"""

    lines = [sentence]
    alt_pattern = r"(?<!])\(([^)]+)\)"
    matches = re.findall(alt_pattern, sentence)

    if len(matches) > 0:
        for block in matches:
            partial_lines = []
            for line in lines:
                for alt in block.split("|"):
                    sen = re.sub(alt_pattern, alt, line, count=1)
                    sen = re.sub(r"\s+", " ", sen)
                    partial_lines.append(sen)

            # If there are multiple alternatives in one sentence, go through the list again
            # This will result in exponential increase of the sentence amount
            lines = partial_lines

    lines = [line.strip() for line in lines]
    return lines


# ==================================================================================================


def build_sentences(nlufile_path: str, output_dir: str) -> None:
    with open(nlufile_path, "r", encoding="utf-8") as file:
        nludata = json.load(file)
    print("Found {} intents".format(len(nludata["intents"].keys())))

    # Remove entity example values and duplicate intents
    nlu_sentences = {}
    for intent in nludata["intents"]:
        sents = []
        for sample in nludata["intents"][intent]:
            # Remove entity example values
            sample = re.sub(r"\[[^\(]*\]", "[---]", sample)
            sents.append(sample)
        nlu_sentences[intent] = list(set(sents))

    # Check that the same utterance doesn't occur in multiple intents
    duplicates = set()
    checked = set()
    for samples in nlu_sentences.values():
        for sample in samples:
            if sample not in checked:
                checked.add(sample)
            else:
                duplicates.add(sample)
    if len(duplicates) > 0:
        msg = "Following sentences ({}/{}) occur in multiple intents: {}"
        print("WARNING:", msg.format(len(duplicates), len(checked), str(duplicates)))

    # Now build sample sentences with expanded alternatives
    print("Expand alternative wordings in the intents ...")
    total_samples = sum((len(v) for _, v in nlu_sentences.items()))
    with tqdm.tqdm(total=total_samples) as pbar:
        for intent, samples in nlu_sentences.items():
            sentences = []

            for sample in samples:
                # Expand alternatives to sentence possibilities
                sents = inject_alternatives(sample)

                sentences.extend(sents)
                pbar.update(1)

            # Remove entity placeholders
            sentences = [s.replace("[---]", "") for s in sentences]

            output_file = os.path.join(output_dir, "{}.txt".format(intent))
            with open(output_file, "w+", encoding="utf-8") as file:
                file.write("\n".join(sentences) + "\n")


# ==================================================================================================


def build_ngram_sentences(workdir: str, nlufile_path: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    build_sentences(nlufile_path, output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--nlufile_path",
        help="Path to a json file with sample intents",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    build_ngram_sentences(args.workdir, args.nlufile_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
