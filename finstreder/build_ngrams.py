import argparse
import os
import subprocess

import tqdm

# ==================================================================================================

input_dirpath = "sents/"
output_dirpath = "ngrams/"
symbols_filepath = "symbols/nlu.syms"

# ==================================================================================================


def build_ngrams(
    input_dir: str, output_dir: str, symbols_path: str, order: int
) -> None:
    print("Building ngram models ...")

    # Build one ngram file for each intent
    files = os.listdir(input_dir)
    for fpath in tqdm.tqdm(files):
        outpath = os.path.join(output_dir, fpath.replace(".txt", ".mod"))

        cmd = "farcompilestrings --fst_type=compact --symbols={} --keep_symbols {} | "
        cmd = cmd + "ngramcount --order={} | ngrammake > {}"

        cmd = cmd.format(symbols_path, os.path.join(input_dir, fpath), order, outpath)
        subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def convert_ngrams(output_dir: str) -> None:
    """Convert ngram to text"""

    print("Converting ngram models to text ...")

    ngram_files = os.listdir(output_dir)
    ngram_files = [f for f in ngram_files if f.endswith(".mod")]

    for fpath in tqdm.tqdm(ngram_files):
        input_file = os.path.join(output_dir, fpath)
        output_file = os.path.join(output_dir, fpath.replace(".mod", ".txt"))

        cmd = "fstprint {} > {}"
        cmd = cmd.format(input_file, output_file)
        subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def build_ngram_fsts(workdir: str, order: int) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    input_dir = os.path.join(workdir, input_dirpath)
    symbols_path = os.path.join(workdir, symbols_filepath)

    build_ngrams(input_dir, output_dir, symbols_path, order)
    convert_ngrams(output_dir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--order",
        help="Directory path for the output files",
        type=int,
        required=True,
    )
    args = parser.parse_args()

    build_ngram_fsts(args.workdir, args.order)


# ==================================================================================================

if __name__ == "__main__":
    main()
