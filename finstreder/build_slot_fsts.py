import argparse
import json
import os
from typing import Dict, Set, Tuple

import tqdm

# ==================================================================================================

output_dirpath = "slots/"

# ==================================================================================================


def check_lookups(lookups: dict) -> None:
    """Check that same slot values don't occur as distinct value and in a synonym replacement"""

    duplicates: Dict[str, Set[str]] = {}
    checked: Dict[str, Set[str]] = {}

    for lookup in lookups:
        duplicates[lookup] = set()
        checked[lookup] = set()

        for sample in lookups[lookup]:
            if "->" in sample:
                val, syn = sample.split("->")
                vals = val[1:-1].split("|")
                vals.append(syn)
                svals = set(vals)
                for v in svals:
                    if v not in checked[lookup]:
                        checked[lookup].add(v)
                    else:
                        duplicates[lookup].add(v)
            else:
                if sample not in checked[lookup]:
                    checked[lookup].add(sample)
                else:
                    duplicates[lookup].add(sample)

    if sum((len(v) for _, v in duplicates.items())) > 0:
        for lookup in dict(duplicates):
            if len(duplicates[lookup]) == 0:
                duplicates.pop(lookup)
        msg = "Following lookup words occur as distinct value and in synonym replacements: "
        raise ValueError(msg + str(duplicates))


# ==================================================================================================


def add_simple_words(
    wordstr: str,
    idx: int,
    start_idx: int = -1,
    end_idx: int = -1,
    eps_input: bool = False,
) -> Tuple[str, int]:
    """Add one or multiple words to the graph"""

    fst_str = ""
    words = wordstr.split(" ")
    words = [w.strip() for w in words]

    for i, word in enumerate(words):
        if eps_input:
            inp_word = "<epsilon>"
        else:
            inp_word = word
            # inp_word = "<epsilon>"

        if i == 0 and start_idx != -1:
            # First word and fixed start id given

            if i == len(words) - 1 and end_idx != -1:
                # Last word as well and fixed end id given
                fst_str += "{} {} {} {}\n".format(start_idx, end_idx, inp_word, word)
            else:
                fst_str += "{} {} {} {}\n".format(start_idx, idx + 1, inp_word, word)
                idx = idx + 1

        elif i == len(words) - 1 and end_idx != -1:
            # Last word and fixed end id given
            fst_str += "{} {} {} {}\n".format(idx, end_idx, inp_word, word)

        else:
            # Standard word in chain
            fst_str += "{} {} {} {}\n".format(idx, idx + 1, inp_word, word)
            idx = idx + 1

    return fst_str, idx


# ==================================================================================================


def build_alternative_states(
    component: str, idx: int, start_idx: int
) -> Tuple[str, int]:
    """Handle components for alternatives or optional words like (opt1|opt2a opt2b| )"""

    # Drop parantheses and split the component
    component = component.strip()
    options = component[1:-1].split("|")
    options = [o.strip() for o in options]

    # Count required extra states to calculate the ending state for all alternatives
    extra_states = 0
    for o in options:
        word_count = len(o.split(" "))
        if word_count > 1:
            # Extra states are only required if the alternative has more than one word
            extra_states += word_count - 1
    end_idx = idx + extra_states + 1

    fst_str = ""
    for option in options:
        if option == "":
            # Alternative is optional
            fst_str += "{} {} <epsilon> <epsilon>\n".format(start_idx, end_idx)
            continue

        fstr, idx = add_simple_words(option, idx, start_idx, end_idx)
        fst_str += fstr

    return (fst_str, end_idx)


# ==================================================================================================


def build_entity_states(
    component: str,
    lookups: dict,
    idx: int,
    start_idx: int,
) -> Tuple[str, int]:
    """Handle components for entities with synonyms like [(val1|val2a val2b|)->val3](entity?e1)"""

    # Extract entity type
    _, entity_tag = component[1:-1].split("](")
    if "?" in entity_tag:
        entity = entity_tag.split("?")[0]
    else:
        entity = entity_tag

    # Count required extra states to calculate the ending state for all entity values
    extra_states = 0
    entity_values = set(lookups[entity])
    for e in entity_values:
        if "->" in e:
            options = e.split("->")[0].strip()[1:-1].split("|")

            for o in options:
                word_count = len(o.split(" "))
                if word_count > 1:
                    # Extra states are only required if the entity value has more than one word
                    extra_states += word_count - 1

            # Extra states for the synonym symbol (->)
            extra_states += 1

            # Extra states for synonym return value
            syn = e.split("->")[1].strip()
            word_count = len(syn.split(" "))
            extra_states += word_count

        else:
            word_count = len(e.split(" "))
            if word_count > 1:
                # Extra states are only required if the entity value has more than one word
                extra_states += word_count - 1

    # One first state for the beginning bracket
    end_idx = 1 + idx + extra_states + 1

    fst_str = ""

    # Start graph with "[" state
    fst_str += "{} {} {} {}\n".format(start_idx, idx + 1, "<epsilon>", "[")
    idx = idx + 1
    start_idx = idx

    for entity in lookups[entity]:
        if "->" in entity:
            # Add value words
            vals, sym = entity.split("->")
            fstr, idx = build_alternative_states(vals, idx, start_idx)
            fst_str += fstr

            # Add "->" state
            fst_str += "{} {} {} {}\n".format(idx, idx + 1, "<epsilon>", "->")
            idx = idx + 1

            # Add synonym words
            fstr, idx = add_simple_words(sym, idx, end_idx=end_idx, eps_input=True)
            fst_str += fstr

        else:
            # Simple entity example
            fstr, idx = add_simple_words(entity, idx, start_idx, end_idx)
            fst_str += fstr

    # End subgraph with "]" transitions
    fst_str += "{} {} {} {}\n".format(end_idx, end_idx + 1, "<epsilon>", "]")
    end_idx = end_idx + 1

    return (fst_str, end_idx)


# ==================================================================================================


def build_slot_fsts(output_dir: str, nlufile_path: str) -> None:
    print("Building slot fsts ...")

    with open(nlufile_path, "r", encoding="utf-8") as file:
        nludata = json.load(file)

    # Check if lookups contain duplicates -> can't optimize them in that case
    check_lookups(nludata["lookups"])

    for lookup in tqdm.tqdm(nludata["lookups"]):
        component = "[---]({})".format(lookup)
        fst_str, idx = build_entity_states(component, nludata["lookups"], 0, 0)
        fst_str += "{}\n".format(idx)

        output_file = os.path.join(output_dir, "{}.txt".format(lookup))
        with open(output_file, "w+", encoding="utf-8") as file:
            file.write(fst_str)


# ==================================================================================================


def create_slot_fsts(workdir: str, nlufile_path: str) -> None:
    output_dir = os.path.join(workdir, output_dirpath)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    build_slot_fsts(output_dir, nlufile_path)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--nlufile_path",
        help="Path to a json file with sample intents",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    create_slot_fsts(args.workdir, args.nlufile_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
