import argparse
import os
import shutil
import time

from finstreder import (
    build_ngram_sentences,
    build_ngrams,
    build_slot_fsts,
    compose_LG_slufsts,
    create_fixedG,
    create_symbols,
    create_TL_fsts,
    extend_ngrams,
    map_intents_slots,
    merge_intent_fsts,
)

# ==================================================================================================


def emtpy_with_ignore(path: str) -> None:
    """Deletes and recreates a directory. Adds a gitignore file to ignore directory contents"""

    if os.path.isdir(path):
        try:
            shutil.rmtree(path)
            os.mkdir(path)
        except OSError as err:
            if len(os.listdir(path)) == 0:
                # In the special case that this directory is mounted as volume it can't be deleted
                # But it will be emptied so the error can be ignored
                pass
            else:
                raise err
    else:
        os.mkdir(path)

    with open(path + ".gitignore", "w+", encoding="utf-8") as file:
        file.write("*\n!.gitignore\n")


# ==================================================================================================


def fullbuild(
    workdir: str, nlufile_path: str, alphabet_path: str, order: int, fixed_grammar: bool
) -> None:
    print("")

    # Delete directory contents, else leftover FSTs might cause errors while merging
    emtpy_with_ignore(workdir)

    create_symbols.create_symbols(workdir, nlufile_path, alphabet_path)
    build_ngram_sentences.build_ngram_sentences(workdir, nlufile_path)
    create_TL_fsts.create_fsts(workdir)

    if fixed_grammar:
        create_fixedG.build_grammar(workdir, "")
    else:
        build_ngrams.build_ngram_fsts(workdir, order)

    extend_ngrams.extend_ngrams(workdir)
    build_slot_fsts.create_slot_fsts(workdir, nlufile_path)
    map_intents_slots.build_map(workdir, nlufile_path)
    merge_intent_fsts.merge_fsts(workdir)
    compose_LG_slufsts.create_fsts(workdir)


# ==================================================================================================


def main() -> None:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument(
        "--workdir",
        help="Path of the working directory",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--nlufile_path",
        help="Path to a json file with sample intents",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--alphabet_path",
        help="Path to the alphabet.json file",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--order",
        help="Order of ngram language model",
        type=int,
        required=False,
        default=0,
    )
    parser.add_argument(
        "--fixed_grammar",
        help="Build fixed instead of ngram grammar",
        action="store_true",
        required=False,
        default=False,
    )
    args = parser.parse_args()

    start = time.time()
    fullbuild(
        args.workdir,
        args.nlufile_path,
        args.alphabet_path,
        args.order,
        args.fixed_grammar,
    )
    finish = time.time()

    print("Building the FSTs took {:.3f} seconds".format(finish - start))


# ==================================================================================================

if __name__ == "__main__":
    main()
