import os

from finstreder import fullbuild_slu

# ==================================================================================================


def test_build_workdirs() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"

    workdir = filepath + "outputs/slu-ngram/"
    if not os.path.isdir(workdir):
        os.makedirs(workdir)


# ==================================================================================================


def test_build_riddles_fixed() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/slu-fixed/"
    nlufile_path = filepath + "data/riddles.json"

    fullbuild_slu.fullbuild(
        workdir,
        nlufile_path,
        alphabet_path=filepath + "data/alphabet_en_chars.json",
        order=0,
        fixed_grammar=True,
    )


# ==================================================================================================


def test_build_riddles_ngram() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/slu-ngram/"
    nlufile_path = filepath + "data/riddles.json"

    fullbuild_slu.fullbuild(
        workdir,
        nlufile_path,
        alphabet_path=filepath + "data/alphabet_en_chars.json",
        order=2,
        fixed_grammar=False,
    )


# ==================================================================================================


def test_build_smartlights_ngram() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/smartlights-ngram/"
    nlufile_path = filepath + "data/smartlights.json"

    fullbuild_slu.fullbuild(
        workdir,
        nlufile_path,
        alphabet_path=filepath + "data/alphabet_en_chars.json",
        order=2,
        fixed_grammar=False,
    )
