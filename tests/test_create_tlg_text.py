import os
import subprocess

from finstreder import create_fixedG, create_symbols, create_TL_fsts

# ==================================================================================================


def test_build_workdirs() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"

    workdir = filepath + "outputs/txt-fixed/"
    if not os.path.isdir(workdir):
        os.makedirs(workdir)

    workdir = filepath + "outputs/txt-sentencepiece/"
    if not os.path.isdir(workdir):
        os.makedirs(workdir)


# ==================================================================================================


def test_create_symbols_txt_chars() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    sentences_path = filepath + "data/sentences_mini.txt"
    alphabet_path = filepath + "data/alphabet_mini_chars.json"
    workdir = filepath + "outputs/txt-fixed/"
    create_symbols.create_symbols(workdir, sentences_path, alphabet_path)


def test_create_symbols_txt_sentencepiece() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    sentences_path = filepath + "data/sentences_mini.txt"
    alphabet_path = filepath + "data/alphabet_mini_sentencepiece.json"
    workdir = filepath + "outputs/txt-sentencepiece/"
    create_symbols.create_symbols(workdir, sentences_path, alphabet_path)


# ==================================================================================================


def test_create_TL_fsts_txt_chars() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/txt-fixed/"
    create_TL_fsts.create_fsts(workdir)


def test_create_TL_fsts_txt_sentencepiece() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/txt-sentencepiece/"
    create_TL_fsts.create_fsts(workdir)


# ==================================================================================================


def test_create_Gfst_txt_chars() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    sentences_path = filepath + "data/sentences_mini.txt"
    workdir = filepath + "outputs/txt-fixed/"
    create_fixedG.build_grammar(workdir, sentences_path)


def test_create_Gfst_txt_sentencepiece() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    sentences_path = filepath + "data/sentences_mini.txt"
    workdir = filepath + "outputs/txt-sentencepiece/"
    create_fixedG.build_grammar(workdir, sentences_path)


# ==================================================================================================


def build_TLG_fst(workdir: str) -> None:
    cdcmd = "cd {} && ".format(workdir)
    if not os.path.isdir(workdir + "results/text/"):
        os.makedirs(workdir + "results/text/")

    cmd = "cat tlg/token.txt | fstcompile --isymbols=symbols/tokens.syms --osymbols=symbols/chars.syms > results/token.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "cat tlg/lexicon.txt | fstcompile --isymbols=symbols/chars.syms --osymbols=symbols/words.syms > tlg/lexicon.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "cat tlg/grammar.txt | fstcompile --isymbols=symbols/words.syms --osymbols=symbols/words.syms > tlg/grammar.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "fstrmepsilon tlg/grammar.fst | fstdeterminize | fstminimize > tlg/grammar_opt.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "fstcompose tlg/lexicon.fst tlg/grammar_opt.fst > tlg/lg.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])

    cmd = "fstrmepsilon tlg/lg.fst | fstdeterminize | fstminimize > results/text/lg_opt.fst"
    subprocess.call(["/bin/bash", "-c", cdcmd + cmd])


# ==================================================================================================


def test_build_TLGfst_txt_chars() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/txt-fixed/"
    build_TLG_fst(workdir)


def test_build_TLGfst_txt_sentencepiece() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/txt-sentencepiece/"
    build_TLG_fst(workdir)
