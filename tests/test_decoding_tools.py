import json
import os
import re
from concurrent.futures import ProcessPoolExecutor as Pool
from typing import Tuple

from finstreder import decoding_tools

try:
    # Import when using pytest
    from . import test_create_inputs
except ImportError:
    # Import when directly calling script
    import test_create_inputs  # type:ignore  # pylint: disable=pointless-statement

# ==================================================================================================


def test_build_input_fst() -> None:
    alphabet = ["<space>", "a", "b", "<blank>"]

    ctc_tokens = [
        [0.1, 0.7, 0.1, 0.1],
        [0.1, 0.1, 0.7, 0.1],
    ]

    fst_label = "0 1 <space> 1.57321 \n \
         0 1 a 0.97321 \n \
         0 1 b 1.57321 \n \
         0 1 <blank> 1.57321 \n \
         1 2 <space> 1.57321 \n \
         1 2 a 1.57321 \n \
         1 2 b 0.97321 \n \
         1 2 <blank> 1.57321 \n \
         2 3 <space> 1.17233 \n \
         2 3 a 1.46916 \n \
         2 3 b 1.46916 \n \
         2 3 <blank> 1.46916 \n \
         3 \n \
        "
    fst_label = re.sub(r"\s*\n\s*", "\n", fst_label)

    fst_str = decoding_tools.build_input_fst(ctc_tokens, alphabet)

    if fst_str != fst_label:
        raise ValueError("Not same:", fst_str, fst_str)


# ==================================================================================================


def test_decoding_text() -> None:
    alphabet = ["<space>", "a", "b", "<blank>"]
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/txt-fixed/"
    path_T = workdir + "results/token.fst"
    path_LGs = workdir + "results/text/"
    path_token_syms = workdir + "symbols/tokens.syms"
    path_word_syms = workdir + "symbols/words.syms"

    # Sequence: "ab ab"
    ctc_tokens = test_create_inputs.ctc_tokens1

    decoder = decoding_tools.Decoder(
        path_LGs, path_T, path_token_syms, path_word_syms, alphabet, fix_alphabet=False
    )

    output = decoder.decode2text(ctc_tokens)
    label = "a bb a"

    if output != label:
        raise ValueError("Not same:", output, label)


# ==================================================================================================


def test_sentencpiece_alphabet() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"

    workdir = filepath + "outputs/txt-sentencepiece/"
    path_T = workdir + "results/token.fst"
    path_LGs = workdir + "results/text/"
    path_token_syms = workdir + "symbols/tokens.syms"
    path_word_syms = workdir + "symbols/words.syms"

    alphabet_path = filepath + "data/alphabet_mini_sentencepiece.json"
    with open(alphabet_path, "r", encoding="utf-8") as file:
        alphabet = json.load(file)

    # Sequence: " bb- bba-a a"
    ctc_tokens = test_create_inputs.ctc_tokens3

    decoder = decoding_tools.Decoder(
        path_LGs, path_T, path_token_syms, path_word_syms, alphabet, fix_alphabet=True
    )

    output = decoder.decode2text(ctc_tokens)
    label = "bb bbaa a"

    if output != label:
        raise ValueError("Not same:", output, label)


# ==================================================================================================


def decode_intent_riddle(workdir: str) -> None:
    path_T = workdir + "results/token.fst"
    path_LGs = workdir + "results/intents/"
    path_token_syms = workdir + "symbols/tokens.syms"
    path_word_syms = workdir + "symbols/nlu.syms"

    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    alphabet_path = filepath + "data/alphabet_en_chars.json"

    with open(alphabet_path, "r", encoding="utf-8") as file:
        alphabet = json.load(file)

    # Sequence: "the solution of the rid-dle is dog"
    ctc_tokens = test_create_inputs.ctc_tokens4

    label = {
        "intent": {
            "name": "skill_riddles-check_riddle",
        },
        "entities": [
            {
                "entity": "skill_riddles-riddle_answers",
                "value": "a pet",
            }
        ],
        "greedy": "the solution is dog",
        "text": "the solution is dog",
    }

    decoder = decoding_tools.Decoder(
        path_LGs, path_T, path_token_syms, path_word_syms, alphabet, fix_alphabet=True
    )
    output = decoder.decode2intent(ctc_tokens)

    if output != label:
        raise ValueError("Not same:", output, label)


# ==================================================================================================


def test_decoding_intent_fixed() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/slu-fixed/"
    decode_intent_riddle(workdir)


def test_decoding_intent_ngram() -> None:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/slu-ngram/"
    decode_intent_riddle(workdir)


# ==================================================================================================


def get_smartlights_components(workdir: str) -> Tuple[decoding_tools.Decoder, dict]:
    path_T = workdir + "results/token.fst"
    path_LGs = workdir + "results/intents/"
    path_token_syms = workdir + "symbols/tokens.syms"
    path_word_syms = workdir + "symbols/nlu.syms"

    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    alphabet_path = filepath + "data/alphabet_en_chars.json"

    with open(alphabet_path, "r", encoding="utf-8") as file:
        alphabet = json.load(file)

    label = {
        "intent": {
            "name": "smartlightsandspeaker_jaco-switchlighton",
        },
        "entities": [
            {
                "entity": "smartlightsandspeaker_jaco-house_room_unique",
                "value": "basement",
            }
        ],
        "greedy": "activate basement lights",
        "text": "activate basement lights",
    }

    decoder = decoding_tools.Decoder(
        path_LGs,
        path_T,
        path_token_syms,
        path_word_syms,
        alphabet,
        fix_alphabet=True,
        weight_scale=22,
        prob_scale=0.5,
        filter_topk=3,
        mean_topk=5,
    )

    return (decoder, label)


# ==================================================================================================


def helper_decoding_intent_smartlights1(_ignore: int) -> bool:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/smartlights-ngram/"
    decoder, label = get_smartlights_components(workdir)
    ctc_tokens = test_create_inputs.ctc_tokens5
    output = decoder.decode2intent(ctc_tokens)
    return output == label


def test_decoding_intent_smartlights1_plain() -> None:
    assert helper_decoding_intent_smartlights1(0)


def test_decoding_intent_smartlights1_threaded() -> None:
    # Test running the parallel decoding step outside of the main thread
    with Pool(1) as p:
        output = list(p.map(helper_decoding_intent_smartlights1, (0,)))
    assert all(output)


# ==================================================================================================


def helper_decoding_intent_smartlights2(_ignore: int) -> bool:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/smartlights-ngram/"
    decoder, label = get_smartlights_components(workdir)
    ctc_tokens = test_create_inputs.ctc_tokens6
    output = decoder.decode2intent(ctc_tokens)
    return output == label


def test_decoding_intent_smartlights2_plain() -> None:
    assert helper_decoding_intent_smartlights2(0)


def test_decoding_intent_smartlights2_threaded() -> None:
    # Test running the parallel decoding step outside of the main thread
    with Pool(1) as p:
        output = list(p.map(helper_decoding_intent_smartlights2, (0,)))
    assert all(output)


# ==================================================================================================


def helper_decoding_text2intent(_ignore: int) -> bool:
    filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
    workdir = filepath + "outputs/smartlights-ngram/"
    decoder, label = get_smartlights_components(workdir)
    text = "activate basement lights"
    output = decoder.text2intent(text)
    return output == label


def test_decoding_text2intent_plain() -> None:
    assert helper_decoding_text2intent(0)


def test_decoding_text2intent_threaded() -> None:
    # Test running the parallel decoding step outside of the main thread
    with Pool(1) as p:
        output = list(p.map(helper_decoding_text2intent, (0,)))
    assert all(output)


# ==================================================================================================

if __name__ == "__main__":
    test_build_input_fst()
    test_decoding_text()
    test_decoding_intent_fixed()
    test_decoding_intent_ngram()
    test_decoding_intent_smartlights1_plain()
    test_decoding_intent_smartlights1_plain()
    test_decoding_intent_smartlights2_plain()
    test_decoding_intent_smartlights2_threaded()
    test_decoding_text2intent_plain()
    test_decoding_text2intent_threaded()
